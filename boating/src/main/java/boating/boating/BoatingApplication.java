package boating.boating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoatingApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoatingApplication.class, args);
    }

}
