package boating.boating.controller;

import boating.boating.forms.BoatForm;
import boating.boating.model.BoatDTO;
import boating.boating.repository.BoatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
public class ListingController {

    private String[] countries = {"Germany", "United Kingdom"};
    private String[] types = {"Boat", "Jet Ski"};
    private String[] brands = {"brandUno", "brandDos"};
    private Integer[] prices = {5000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000};
    @Autowired
    private BoatRepository boatRepository;

    @GetMapping("/create")
    public ModelAndView displayCreation() {
        ModelAndView mav = new ModelAndView("Creation");
        mav.addObject("countries",countries);
        return mav;
    }

    @PostMapping("/create")
    public ModelAndView createListing(BoatForm boatForm,BindingResult br){
    if (br.hasErrors()) {
        System.out.println("Error");
    }
     BoatDTO boat = new BoatDTO(boatForm);
        boatRepository.save(boat);
        return new ModelAndView("HomePage");
    }

    @GetMapping("/listings")
    public ModelAndView displayListings() {
//        https://www.baeldung.com/spring-data-jpa-pagination-sorting
        int i = 0;

        ModelAndView mav = new ModelAndView("Listings");
        Pageable firstPageWithTwoElements = PageRequest.of(0,1);
        Page<BoatDTO> boats = boatRepository.findAll(firstPageWithTwoElements);

        mav.addObject("listings",boats);
        mav.addObject("pages",boats.getTotalPages());
        mav.addObject("totalListings",boats.getTotalElements());
        mav.addObject("index",i);
        return mav;
    }
    @RequestMapping(path = "/listing", method = RequestMethod.GET)
    public ModelAndView displayListingsPage(@RequestParam(name = "page", required = false) Optional<Integer> page,
                                            @RequestParam(name = "sort", required = false) Optional<String> sort,
                                            @RequestParam(name = "desc", required = false) Optional<Integer> desc,
                                            @RequestParam(name = "priceto", required = false) Optional<Integer> priceto,
                                            @RequestParam(name = "country", required = false) Optional<String> country,
                                            @RequestParam(name = "brands", required = false) Optional<String> brand,
                                            @RequestParam(name = "type", required = false) Optional<String> type ) {
//        https://www.baeldung.com/spring-data-jpa-pagination-sorting

        Pageable pageable = null;
        ModelAndView mav = new ModelAndView("Listings");
        int queryPage = 0;
        int pageSize = 20;

        // Some logic which page should be displayed
        if (page.isPresent()) {
            queryPage = page.get() - 1;
        } else {
            queryPage = 0;
        }

        // Set sorting params
        if (sort.isPresent()) {
            String[] split = sort.get().split("_");

            switch (split[split.length-1]) {
                case "asc": {
                    pageable = PageRequest.of(queryPage,pageSize,Sort.by(Sort.Direction.ASC,split[0]));
                    break;
                }
                case "desc": {
                    pageable = PageRequest.of(queryPage,pageSize,Sort.by(Sort.Direction.DESC ,split[0]));
                    break;
                }
            }
        } else {
            pageable = PageRequest.of(queryPage,pageSize);
        }

        // Query
        Page<BoatDTO> boats = boatRepository.findBoatBy(priceto, country, brand, type,pageable);


        //Pass objects to model
        mav.addObject("listings",boats);
        mav.addObject("pages",boats.getTotalPages());
        mav.addObject("totalListings",boats.getTotalElements());

        // Query params
        mav.addObject("pricetoParam",priceto);
        mav.addObject("countryParam",country);
        mav.addObject("brandParam",brand);
        mav.addObject("typeParam",type);
        // Query options
        mav.addObject("pricetoQuery",prices);
        mav.addObject("countriesQuery",countries);
        mav.addObject("brandsQuery",brands);
        mav.addObject("typeQuery",types);
        return mav;
    }

    @GetMapping("/listings/{id}")
    public ModelAndView displayListingDetail( @RequestParam("id") int id){
        ModelAndView mav = new ModelAndView("ListingDetail");
        mav.addObject("listing",boatRepository.findById(id));
        return mav;
    }
}
