package boating.boating.controller;

import boating.boating.forms.RegisterForm;
import boating.boating.model.UserDTO;
import boating.boating.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LoginController {

    private UserRepository userRepo;

    @Autowired
    public LoginController(UserRepository pRepo) { userRepo = pRepo;}


    @RequestMapping(path = "/login")
    public ModelAndView returnLoginPage() {


        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");
        return mav;
    }

    @RequestMapping(path = "/Register")
    public ModelAndView returnRegister() {


        ModelAndView mav = new ModelAndView();
        mav.setViewName("Register");
        return mav;
    }

    @RequestMapping(path = "/RegisterUser")
    public ModelAndView registerUser(RegisterForm registerForm) {

        userRepo.save(new UserDTO(registerForm.getMail(),registerForm.getName(),registerForm.getPassword(),"user"));
        return returnLoginPage();
    }

//    @RequestMapping(path = "/myLogin", method = RequestMethod.POST)
//    public ModelAndView processLogin() {
//
//        return new ModelAndView("HomePage");
//    }
}
