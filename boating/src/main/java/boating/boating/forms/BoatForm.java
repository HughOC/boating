package boating.boating.forms;

import org.springframework.web.multipart.MultipartFile;

public class BoatForm {
    private String name;
    private int price;
    private String city;
    private String country;
    private MultipartFile[] files;


    public BoatForm(String name, int price, String city, String country, MultipartFile[] files) {
        this.name = name;
        this.price = price;
        this.city = city;
        this.country = country;
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public MultipartFile[] getFile() {
        return files;
    }
}
