package boating.boating.model;

import boating.boating.forms.BoatForm;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Entity
public class BoatDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String type;
    private String brand;
    private String country;
    private String city;

    @Column(name = "price")
    private int price;
    private String email;

    private boolean active;
//    private Date

    // Cascade all saves the child data when parent gets saved
    @OneToMany(targetEntity = Image.class, cascade = CascadeType.ALL)
    private List<Image> images;
//    @JoinTable(name = "images",
//            joinColumns = @JoinColumn(name = "id"),
//            inverseJoinColumns = @JoinColumn(name = "boatingid"))
    @Lob
    private byte[] image;
    private String mediaType;

    // Optional args
    private int phone;
    private String contactName;

    // More optional data
    private String condition;
    private String material;
    private String description;
    private int manufacturingYear;
    private int length;
    private int width;
    private String engine;
    private String drive;
    private int power;

    private int personCapacity;
    private int cabins;
    private int bunks;

    private int freshWater;
    private int wasteTank;

    public BoatDTO() {
    }

    public BoatDTO(BoatForm boatForm) {
        this.type = boatForm.getName();
        this.price = boatForm.getPrice();
        this.city = boatForm.getCity();
        this.country = boatForm.getCountry();

        this.images = new ArrayList<>();
        for (MultipartFile image : boatForm.getFile()) {

            try {
                images.add(new Image(image.getBytes(),image.getContentType()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public int getPrice() {
        return price;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActive() {
        return active;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getMediaType() {
        return mediaType;
    }

    public int getPhone() {
        return phone;
    }

    public String getContactName() {
        return contactName;
    }

    public String getCondition() {
        return condition;
    }

    public String getMaterial() {
        return material;
    }

    public String getDescription() {
        return description;
    }

    public int getManufacturingYear() {
        return manufacturingYear;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public String getEngine() {
        return engine;
    }

    public String getDrive() {
        return drive;
    }

    public int getPower() {
        return power;
    }

    public int getPersonCapacity() {
        return personCapacity;
    }

    public int getCabins() {
        return cabins;
    }

    public int getBunks() {
        return bunks;
    }

    public int getFreshWater() {
        return freshWater;
    }

    public int getWasteTank() {
        return wasteTank;
    }


}
