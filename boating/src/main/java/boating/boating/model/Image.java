package boating.boating.model;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;

@Entity
//@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Lob
    private byte[] blob;
    private String mediaType;



    public Image(byte[] blob, String mediaType) {
        this.blob = blob;
        this.mediaType = mediaType;
    }

    public Image() {

    }

    public byte[] getBlob() {
        return blob;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getBufferedImage() throws SQLException, IOException {
        byte[] encodeBase64 = Base64.getEncoder().encode(this.getBlob());
        String base64Encoded = new String(encodeBase64, "UTF-8");
        return base64Encoded;
    }
}
