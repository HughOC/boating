package boating.boating.repository;

import boating.boating.model.BoatDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


import java.util.Optional;

public interface BoatRepository extends PagingAndSortingRepository<BoatDTO,Integer> {

    @Query(value = "SELECT * from BoatDTO boat where (:priceto is null or boat.price < :priceto) and" +
            "(:country is null or boat.country = :country) and" +
            "(:brand is null or boat.brand = :brand) and"  +
            "(:type is null or boat.type = :type)",
            nativeQuery = true)
     Page<BoatDTO> findBoatBy(
            @Param("priceto") Optional<Integer> priceto,
            @Param("country") Optional<String> country,
            @Param("brand") Optional<String> brand,
            @Param("type") Optional<String> type,
            Pageable pageable);
}
