package boating.boating.repository;

import boating.boating.forms.RegisterForm;
import boating.boating.model.UserDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<UserDTO, Integer> {

    @Query("SELECT u from UserDTO u where u.email = ?1")
    public UserDTO findUserByMail(String email);


}
