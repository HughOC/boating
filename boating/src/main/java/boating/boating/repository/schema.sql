Create schema IF NOT EXISTS boating;
use boating;


drop table if exists user;
drop table if exists user_role;


-- Create table
create table user
(
  USER_ID BIGINT not null primary key auto_increment,
  USER_MAIL VARCHAR(36) not null,
  USER_NAME text,
  ENCRYPTED_PASSWORD VARCHAR(128) not null,
  ENABLED BIT not null
) ;
--
alter table user auto_increment =1;


alter table user
  add constraint user_uk unique (USER_MAIL);

-- Create user with passwd 123
insert into user(USER_ID,USER_MAIL,ENCRYPTED_PASSWORD,ENABLED)
values (1, 'user', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 1);

-- Optional Table user linking
-- create table role
-- (
--   ROLE_ID   BIGINT not null,
--   ROLE_NAME VARCHAR(30) not null
-- ) ;
-- --
-- alter table role
--   add constraint ROLE_PK primary key (ROLE_ID);
--
-- alter table role
--   add constraint ROLE_UK unique (ROLE_NAME);


-- Create table
create table user_role
(
  ID BIGINT not null primary key auto_increment,
  USER_MAIL VARCHAR(36) not null,
  ROLE_NAME VARCHAR(30) not null
);
--
alter table user_role auto_increment=1;


alter table user_role
  add constraint user_role_uk unique (USER_MAIL);

