package boating.boating.service;

import boating.boating.model.UserDTO;
import boating.boating.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

@Service("userDetailsService")
public class CustomUserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        UserDTO user =  userRepository.findUserByMail(username);
        if (user != null) {
            String password = user.getPassword();
            Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

            authorities.add(new SimpleGrantedAuthority(user.getUserRole()));

            User securedUser = new User(username, password, authorities);
            return securedUser;
        } else {
            throw new UsernameNotFoundException(
                    "User not found");
        }
    }
}
